package Question2;
public class Vehicle {

	public String name;
	public long cost;
	public String fuel;
	public float maxSpeed;
	public String transportationMedium;
	

	public Vehicle(String name, long cost, String fuel, float maxSpeed, String transportationMedium) {
		this.name = name;
		this.cost = cost;
		this.fuel = fuel;
		this.maxSpeed = maxSpeed;
		this.transportationMedium = transportationMedium;
	}
	
	public void display() {
		System.out.println("Vehicle name:" + name);
		System.out.println("Cost:" +cost);
		System.out.println("Fuel type:"+ fuel);
		System.out.println("Maximum speed:" +maxSpeed);
		System.out.println("Transportation medium: " + transportationMedium);
	}
	
public static void main(String args[]) {
		
		PublicTransport PT = new PublicTransport("Bus", 50000, "Diesle", 120, "Road", 52, "NRQ");
		PT.service();
		PT.display();
		
		PersonalVehicle personal = new PersonalVehicle("Car", 45000, "Electric",150 ,"Road","Rose Jane", "WHJE");
		personal.display();
		personal.buy();
		personal.sell();
		
		MilitaryVehicle military = new MilitaryVehicle("Fighter Jet", 500000, "gasoline", 400, "Air", "Missile bomb");
		military.attack();
		military.defend();
}
	
}
