package Question2;
public class PersonalVehicle extends Vehicle {


	private String ownerName;
	public String registrationNum;
	

	public PersonalVehicle(String name, long cost, String fuel, float maxSpeed, String transporationMedium, String ownerName, String registrationNum) {
		super(name, cost, fuel, maxSpeed, transporationMedium);
		this.ownerName= ownerName;
		this.registrationNum= registrationNum;
	}

	public void buy() {
		System.out.println("You can buy personal vehicle.");
	}
	
	public void sell() {
		System.out.println("You can sell personal vehicle.");
	}
}
