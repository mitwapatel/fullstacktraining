package Question2;
public class PublicTransport extends Vehicle{

	int maxCapacity;
	String registrationNum;
	
	public PublicTransport(String name, long i, String fuel, float maxSpeed, String transporationMedium,int maxCapacity, String registrationNum) {
		super(name, i, fuel, maxSpeed, transporationMedium);
		// TODO Auto-generated constructor stub
		this.maxCapacity = maxCapacity;
		this.registrationNum = registrationNum;
	}

	public void service() {
		System.out.println("This vehicle is using for public service");
	}
}
