package Question2;
public class MilitaryVehicle extends Vehicle {

	private String weaponLoad;

	public MilitaryVehicle(String name, long cost, String fuel, float maxSpeed, String transporationMedium,
			String weaponLoad) {
		super(name, cost, fuel, maxSpeed, transporationMedium);
		this.weaponLoad = weaponLoad;
	}

	public void attack() {
		System.out.println("This military vehicle is using for attack.");
	}

	public void defend() {
		System.out.println("This military vehicle is using for defend.");
	}
	
	
}
